+++
title = "Project: [DUKE] IDS721 MiniProject6"
template = "page.html"
date = 2024-03-04
[taxonomies]
tags = ["Project"]
[extra]
summary = "This project focuses on enhancing a Rust-based AWS Lambda function with advanced logging and tracing capabilities."
mathjax = "tex-mml"
+++

<!-- more -->
# Rust Lambda Function with Enhanced Logging and Tracing

## Project Overview

This project focuses on enhancing a Rust-based AWS Lambda function with advanced logging and tracing capabilities. The aim is to integrate AWS X-Ray and CloudWatch to provide in-depth insights into the function's execution, error handling, and performance metrics.

## Functionality Description

The Lambda function developed for this project calculates the factorial of an integer:

- `number`: integer

It returns the factorial of these numbers.

## Detailed Steps

### Implementation

1. **Project Creation**: Use `cargo lambda new <Project Name>` to create a new project. Replace `<Project Name>` with your desired project name.
2. **Function Development**: Start developing your Lambda function in `main.rs` and add necessary dependencies in `Cargo.toml`.

1. **AWS IAM User**: Create an AWS IAM user with `iamfullaccess`, `lambdafullaccess`, `AmazonXrayFullAccess` permissions. Securely store the generated `Access Key`.

2. **Dependencies**: Include `tracing` and `tracing-subscriber` in your `Cargo.toml` for logging and tracing capabilities.
3. **Makefile**: Add a `Makefile` to simplify your build and deployment process. Adjust it according to your function's needs.
4. **Function Testing**: Test your function's output with `cargo lambda invoke --data-ascii "{ \"command\": \"encrypt\", \"message\": \"encrypt\" }"` while `cargo lambda watch` is running.
5. **Environment Setup**: Create a `.env` file for AWS credentials and region, and a `.gitignore` to exclude `/target` and `.env`. Configure your environment variables with `Export` or `set -a; source .env`.
6. **Deployment**: Deploy your function using `cargo lambda build` and `cargo lambda deploy`.
7. **AWS Console Configuration**: Enable AWS X-Ray Active tracing and CloudWatch Lambda Insights Enhanced monitoring in the Lambda function's configuration on AWS Console.
8. **Remote Testing**: Test your function either in AWS Lambda or remotely with `cargo lambda invoke --remote idsweek6 --data-ascii "{ \"number\": 7}"`, adjusting the command accordingly.
9. **Monitoring**: Access trace and log details in the Monitor section of your Lambda function, which links to AWS CloudWatch for detailed analytics.
10. **API Gateway Setup**: Establish a REST API via API Gateway with the `ANY` method type. After deployment, your API endpoint should resemble: `https://hrwpw12b50.execute-api.us-east-1.amazonaws.com/mini6`
11. **API Testing**:
    ```shell
    curl -X POST https://fyvwedkfre.execute-api.us-east-1.amazonaws.com/factorial_stage/factorial \
      -H 'content-type: application/json' \
      -d '{ "number": 7}'
    ```
    Adjust the URL and payload as necessary for your API testing.


## Screenshots

- Successfully invoke
![1](https://gitlab.com/Johnnyee/ids721project6/-/wikis/uploads/689fa366efda950b7cf3b64d8bf601d0/1.png)
![2](https://gitlab.com/Johnnyee/ids721project6/-/wikis/uploads/d1acfd8dc3c0f40f6d96b3ebf5134074/2.png)

- CloudWatch and X-Ray
![3](https://gitlab.com/Johnnyee/ids721project6/-/wikis/uploads/44b1ca581cb402b98f39b0aae303de43/3.png)

- Tracing and logging
![4](https://gitlab.com/Johnnyee/ids721project6/-/wikis/uploads/1101e22fff3aea9e37f1f2ca97cfd8fb/4.png)
