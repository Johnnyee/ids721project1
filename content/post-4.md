+++
title = "Project: [DUKE] IDS721 MiniProject4"
template = "page.html"
date = 2024-02-22
[taxonomies]
tags = ["Project"]
[extra]
summary = "This project involves developing a straightforward web application using Rust and the Actix web framework."
mathjax = "tex-mml"
+++

<!-- more -->
# Rust Actix Web Application
## Project Overview
This project involves developing a straightforward web application using `Rust` and the Actix web framework. The key task is to containerize a basic Rust Actix web application, construct a Docker image, and execute the container on a local machine. Installing `Docker Desktop` is a prerequisite for running the container locally.

## Functionality Description
The core functionality of this web application is a Rust function within `main.rs` designed to choose a lucky number. Upon user interaction with the `CLICK HERE` button, the application randomly selects a number between 1 and 100 to display on the webpage.

## Implementation Steps
1. Launch the terminal and execute `cargo new <YOUR PROJECT NAME>` to initiate a new Rust project.
2. Navigate to `/src/main.rs` and implement your function for the web app. Ensure to import `use actix_web::{web, App, HttpResponse, HttpServer, Responder}` and `use actix_files::Files` at the beginning of the file.
3. To enhance the website's user interface, establish a `static` directory and within it, create an `index.html` file. Populate this file with the necessary HTML and CSS code. Then, within `main.rs`, integrate `.service(Files::new("/", "./static").index_file("index.html"))` into `App::new()` to apply your styling to the web application.
4. Amend `Cargo.toml` to include essential dependencies (or additional ones based on your application's requirements):
    - `actix-web`: Rust's web framework
    - `actix-files`: for serving static files
    - `rand`: for random number generation in Rust
5. Generate a `Dockerfile` and a `Makefile` suitable for your project. You may use my versions as a template, but ensure that your project's name in the `Dockerfile` and the image name in the `Makefile` correspond with the `name` specified in `Cargo.toml`.
6. With `Docker Desktop` running, open a terminal, change to your project directory, and execute `cargo build` followed by `cargo run`. Concurrently, in another terminal window, execute `docker build -t <YOUR IMAGE NAME> .` to build your Docker image. Your web application should now be accessible at `http://localhost:8080/`.
7. To run your Docker container, execute `docker run -d -p 8080:8080 <YOUR IMAGE NAME>`. You can then view your running container within `Docker Desktop` by navigating to the `Containers` section and opening the designated `Port`.

## Project Screenshot

- Website

![Web 1](https://gitlab.com/Johnnyee/ids721project4/-/wikis/uploads/154e5ea6bf2a50432928da3307691875/website.png)
![Web 2](https://gitlab.com/Johnnyee/ids721project4/-/wikis/uploads/6743be1bda667e20eb228f7e0346870e/images.png)

- Docker Container

![Container](https://gitlab.com/Johnnyee/ids721project4/-/wikis/uploads/e7505913d49820d79bfa21a836e0302a/container.png)

