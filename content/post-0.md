+++
title = "Econ Honor Thesis"
template = "page.html"
date = 2024-01-29
[taxonomies]
tags = ["Thesis"]
[extra]
summary = "UCSB students undertake independent research project(s) under the direction of a faculty member. The research results are presented as an honors paper."
+++

---

### Thesis Screenshots:
![1](https://gitlab.com/Johnnyee/ids721project3/-/wikis/uploads/0a306646444c53236dce7aa47b838b1d/WechatIMG839.png)

---

![1](https://gitlab.com/Johnnyee/ids721project3/-/wikis/uploads/67a4f75cc0f88dc404cc7f95305d7364/WechatIMG840.png)

---

![1](https://gitlab.com/Johnnyee/ids721project3/-/wikis/uploads/ce6c64d0527bb782f9e8f66d77d9388d/WechatIMG841.png)

#### Check it if you are interested in: [The Effects of the COVID-19 Pandemic on the Digital Divide in the United States](https://gitlab.com/Johnnyee/ids721project3/-/wikis/uploads/0c3ec6e15942b40e6f10553f7e4a7099/Honor_Thesis__Yujie_Ye_.pdf)

