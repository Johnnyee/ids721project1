+++
title = "Project: [DUKE] IDS721 MiniProject5"
template = "page.html"
date = 2024-02-26
[taxonomies]
tags = ["Project"]
[extra]
summary = "This project creates a Rust-based AWS Lambda function generating a random lucky number (from 1 to 100) for a user and stores it in an AWS DynamoDB table. "
mathjax = "tex-mml"
+++

<!-- more -->
# Lucky Number Generator Lambda Function

## Overview

This Rust-based AWS Lambda function generates a random lucky number (from 1 to 100) for a user and stores it in an AWS DynamoDB table. It demonstrates integration with AWS services like DynamoDB and Lambda, processing incoming requests and returning a lucky number as a response.

## Prerequisites

- AWS CLI configured with necessary permissions.
- Rust and Cargo installed on your machine.

## Dependencies

This project uses several Rust crates:

- `aws-sdk-dynamodb` for DynamoDB interaction.
- `lambda_runtime` for the AWS Lambda Rust runtime.
- `serde` and `serde_json` for JSON serialization/deserialization.
- `tokio` for the async runtime.
- `tracing` and `tracing-subscriber` for logging and tracing.
- `rand` for random number generation.

## Setup

### 1. Compile the Project

Ensure your Rust environment is ready, navigate to the project directory, and build with Cargo:

```bash
cargo build --release
```

### 2. Create the DynamoDB Table
Create a DynamoDB table named `LuckyNum` with a primary key `user_id` of type String.


### 3. Deploy the Lambda Function
Deploy your function to AWS Lambda using the AWS CLI or Management Console, ensuring the execution role has DynamoDB access.

### 4. Set Environment Variables
Configure environment variables for AWS credentials and region as needed by the AWS SDK.
### 5. Invoke the Lambda Function
```json
{
  "command": "generateLuckyNumber"
}
```

### Invocation Example
```json
{
  "req_id": "unique-request-id",
  "msg": "You rolled a 42"
}
```

## More Detailed Process


Follow these steps to create and deploy a Rust-based AWS Lambda function:

1. **Prerequisites Installation**:
    - Install Rust and Cargo Lambda using Homebrew with the commands:
      ```
      brew install rust
      brew tap cargo-lambda/cargo-lambda
      brew install cargo-lambda
      ```

2. **AWS Account Setup**:
    - If you don't have one, create a free AWS account.

3. **Project Initialization**:
    - Initialize a new project with Cargo Lambda:
      ```
      cargo lambda new my-lambda-function
      ```

4. **Project Customization**:
    - Customize the generated project template as required.

5. **Local Testing**:
    - Use the following command to test your function locally:
      ```
      cargo lambda watch
      ```

6. **Invoke Test**:
    - Test the Lambda function with a sample invocation:
      ```
      cargo lambda invoke --data-ascii "{\"command\": \"encrypt\", \"message\": \"encrypt this\"}"
      ```

7. **AWS IAM Configuration**:
    - Navigate to the AWS IAM Management Console and create a new IAM user with necessary credentials.
    - Attach the `LambdaFullAccess` and `IAMFullAccess` policies to the user.

8. **Security Credentials**:
    - Complete the IAM user setup and retrieve your security credentials.

9. **Access Keys**:
    - Generate an access key for the IAM user.

10. **Environment File**:
    - Store the `AWS_ACCESS_KEY_ID`, `AWS_SECRET_ACCESS_KEY`, and `AWS_REGION` in a `.env` file. Do not commit this file to your repository; add it to `.gitignore`.

11. **Deployment**:
    - Deploy your Lambda function with:
      ```
      cargo lambda deploy
      ```

12. **AWS Management Console**:
    - Log in to AWS Lambda and ensure your function appears correctly. Add permissions and configurations as needed.

13. **DynamoDB Integration**:
    - If your Lambda function interacts with DynamoDB, grant it the necessary permissions by attaching the `AmazonDynamoDBFullAccess` policy. If a new table is needed, create one and set up the primary key.

14. **API Gateway Setup**:
    - Set up an API Gateway to expose your Lambda function as an HTTP endpoint. Use default settings for a new REST API and connect it to your Lambda function.

15. **API Configuration**:
    - Configure the API method as `ANY` and integrate it with your Lambda function.

16. **Deployment and Testing**:
    - Deploy your API changes and test the function using the provided invoke URL.
    - For example, in my project, the invoke URL is: https://0e65nblob5.execute-api.us-west-2.amazonaws.com/luckynum/luckynum
    - We can also test our lambda function without API gateway: `cargo lambda invoke --remote <name of lambda function>`

Remember to replace `my-lambda-function` with your actual project name and modify the steps according to your project's specifics.

## Some Screenshots
DynamoDB1

![DynamoDB1](https://gitlab.com/Johnnyee/ids721project5/-/wikis/uploads/2c0df30d81ad51ced3e5ed4eb415864d/WechatIMG76.png)

DynamoDB2

![DynamoDB2](https://gitlab.com/Johnnyee/ids721project5/-/wikis/uploads/77d295e3e1862748ff55d0afe0a64853/WechatIMG77.png)

Generated by Invoke URL

![Generated by Invoke URL](https://gitlab.com/Johnnyee/ids721project5/-/wikis/uploads/1fbeb5a3a7ac0fb929493b5e080fe5d5/WechatIMG78.png)

Lambda Function

![Lambda Function](https://gitlab.com/Johnnyee/ids721project5/-/wikis/uploads/375611f85737635751c76a921e10da6e/WechatIMG79.png)


