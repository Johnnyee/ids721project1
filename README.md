# MiniProject1 & Individual Project1: Zola Static Site

## MiniProject1

MiniProject1 talks about how to use ZOLA to build a website and deploy it via GitLab. ZOLA is a fast static site generator in a single binary with everything built-in, making it an excellent choice for building websites.

Here is my personal website: [Johnny's Website](https://ids721project1-johnnyee-156b0501db6ce7944b3100609e76ae68be1279b.gitlab.io/)

## Screenshot of Website

![1](https://gitlab.com/Johnnyee/ids721project1/-/wikis/uploads/037ce63be8347df8b0def34d6b4356c9/website.png)

## Demo Video
I stored my demo vide in google drive.
```
https://drive.google.com/file/d/1cJ47vf3hIUJoKUGlHvFDgQq_Ygiaz5bQ/view?usp=sharing
```

## Prerequisites

Before you begin, ensure you have the following installed:
- ZOLA: [Installation Guide](https://www.getzola.org/documentation/getting-started/installation/)
- Git: [Installation Guide](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Step 1: Setting Up Your ZOLA Project

1. Create a new ZOLA project:
   ```shell
   zola init my_website
   ```
2. Navigate to your project directory:
   ```shell
   cd my_website
   ```
3. Start the ZOLA server to preview your site locally (access via http://localhost:1111):
   ```shell
   zola serve
   ```

## Step 2: Building Your Site

Once you are satisfied with your site, build the static files:
```shell
zola build
```
This command generates a `public` directory containing your site's static files.

## Step 3: Setting Up a GitLab Repository

1. Sign up or log in to your GitLab account.
2. Create a new repository for your project.
3. Note your repository's URL for the next steps.

## Step 4: Initializing a Git Repository

1. Initialize a Git repository in your ZOLA project folder if you haven't already:
   ```shell
   git init
   ```
2. Add the GitLab repository as a remote:
   ```shell
   git remote add origin YOUR_REPOSITORY_URL
   ```

## Step 5: Pushing Your Site to GitLab

1. Add your site's files to the repository:
   ```shell
   git add .
   ```
2. Commit the changes:
   ```shell
   git commit -m "Initial ZOLA site commit"
   ```
3. Push your site to GitLab:
   ```shell
   git push -u origin master
   ```

## Conclusion

You've now successfully built a website with ZOLA and deployed it to GitLab. For further customization and more advanced features, refer to the [ZOLA Documentation](https://www.getzola.org/documentation/).

---
---

## Individual Project 1

In Individual Project 1, we polish our website and add IDS721 miniproject 1-6 in the website. And we also edit our style.

## Prerequisites

Before you begin, ensure you have the following:

- A working Zola installation (Refer to [Zola's documentation](https://www.getzola.org/documentation/getting-started/installation/) for installation instructions).
- Basic knowledge of HTML and CSS.
- A text editor of your choice (VS Code, Sublime Text, etc.).

## Understanding Zola's Structure

Zola sites are made up of templates (written in Tera, Zola's templating engine), content (written in Markdown), and static files (like images and stylesheets). The stylesheets are what we'll focus on editing.

## Editing the Style

### Locating the CSS Files

Your Zola project's theme, if it uses one, will typically have its CSS files located in the `themes/[your-theme]/sass` or `themes/[your-theme]/css` directory. If you're not using a theme, or if you've created your site from scratch, your CSS files will likely be in the `static/css` directory.

### Modifying CSS

To modify the style:

1. Navigate to the directory where your CSS files are located.
2. Open the CSS file you wish to edit in your text editor.
3. Make your desired changes. This might include altering colors, fonts, spacing, or layout.
4. Save your changes.

### Using Sass or SCSS

If your site uses Sass/SCSS:

1. The process is similar to editing CSS. Locate the `.sass` or `.scss` files in the same directories.
2. After editing, you'll need to compile your Sass/SCSS to CSS. This can be done using Zola's built-in Sass compiler. Run `zola build` in your project directory, and Zola will automatically compile the Sass/SCSS files into CSS.

## Applying Changes

To see your changes, run `zola serve` in your project directory. This command builds your site and serves it on a local web server, usually at `http://localhost:1111`. Refresh your browser to see the updates.

## Troubleshooting Common Issues

- **Changes not appearing**: Make sure you've saved your CSS/Sass files and rerun `zola serve`. Clear your browser cache if necessary.
- **CSS syntax errors**: Validate your CSS with a tool like [W3C CSS Validator](https://jigsaw.w3.org/css-validator/) to ensure there are no syntax errors.

## Resources

- [Zola Documentation](https://www.getzola.org/documentation/)
- [Tera Template Documentation](https://tera.netlify.app/docs/)
- [MDN Web Docs (for CSS and HTML)](https://developer.mozilla.org/)

For more help, consider asking questions on [Zola's discussion forum](https://zola.discourse.group/) or reaching out to the community on social media.
