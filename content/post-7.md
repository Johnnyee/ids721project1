+++
title = "Duke Video"
template = "page.html"
date = 2024-01-10
[taxonomies]
tags = ["Video"]
[extra]
summary = "100 Years of Duke"
+++

Celebrating 100 years of Duke University is not just a commemoration of its past; it's a vibrant testament to a century of academic excellence, groundbreaking research, and a profound commitment to community and global impact. Being a Duke student during this centennial moment offers a unique perspective on the legacy and future of this prestigious institution.

Being happy to share this video stems from a sense of pride in Duke's legacy—a chance to showcase the university's commitment to excellence in education, research, and public service. It's about highlighting the community of scholars, students, and alumni who have contributed to making Duke a beacon of knowledge, innovation, and leadership on the global stage.


## Youtube - 100 Years of Duke
{{ youtube(id="5OxycSTGWOk", autoplay=true) }}
