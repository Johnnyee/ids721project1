+++
title = "About"
template = "about.html"
paginate_by = 0
+++

<img src="HP.JPG" alt="headphoto" width="200"/>

### CONTACT

- #### Email: yy413@duke.edu

- #### Address: Duke University, Durham, NC, USA


### EDUCATION

- #### Master of Science in Statistical Science at Duke University, 2023-2025

- #### Bachelor of Art in Economics and minor in Statistics at University of California, Santa Barbara, 2019-2023

### Interest

- #### Being a data scientist
- #### Swimming 🏊 and snowboarding 🏂