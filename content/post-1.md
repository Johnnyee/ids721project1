+++
title = "Project 1: Create a personal website by ZOLA"
template = "page.html"
date = 2024-01-30
[taxonomies]
tags = ["Project"]
[extra]
summary = "Create a personal website by ZOLA"
mathjax = "tex-mml"
+++



# Building and Deploying a Website with ZOLA and GitLab

This project talks about how to use ZOLA to build a website and deploy it via GitLab. ZOLA is a fast static site generator in a single binary with everything built-in, making it an excellent choice for building websites.

Here is my personal website: [Johnny's Website](https://ids721project1-johnnyee-156b0501db6ce7944b3100609e76ae68be1279b.gitlab.io/)

## Prerequisites

Before you begin, ensure you have the following installed:
- ZOLA: [Installation Guide](https://www.getzola.org/documentation/getting-started/installation/)
- Git: [Installation Guide](https://git-scm.com/book/en/v2/Getting-Started-Installing-Git)

## Step 1: Setting Up Your ZOLA Project

1. Create a new ZOLA project:
   ```shell
   zola init my_website
   ```
2. Navigate to your project directory:
   ```shell
   cd my_website
   ```
3. Start the ZOLA server to preview your site locally (access via http://localhost:1111):
   ```shell
   zola serve
   ```

## Step 2: Building Your Site

Once you are satisfied with your site, build the static files:
```shell
zola build
```
This command generates a `public` directory containing your site's static files.

## Step 3: Setting Up a GitLab Repository

1. Sign up or log in to your GitLab account.
2. Create a new repository for your project.
3. Note your repository's URL for the next steps.

## Step 4: Initializing a Git Repository

1. Initialize a Git repository in your ZOLA project folder if you haven't already:
   ```shell
   git init
   ```
2. Add the GitLab repository as a remote:
   ```shell
   git remote add origin YOUR_REPOSITORY_URL
   ```

## Step 5: Pushing Your Site to GitLab

1. Add your site's files to the repository:
   ```shell
   git add .
   ```
2. Commit the changes:
   ```shell
   git commit -m "Initial ZOLA site commit"
   ```
3. Push your site to GitLab:
   ```shell
   git push -u origin master
   ```

## Conclusion

You've now successfully built a website with ZOLA and deployed it to GitLab. For further customization and more advanced features, refer to the [ZOLA Documentation](https://www.getzola.org/documentation/).