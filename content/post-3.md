+++
title = "Project: [DUKE] IDS721 MiniProject3"
template = "page.html"
date = 2024-02-15
[taxonomies]
tags = ["Project"]
[extra]
summary = "This project leverages the AWS Cloud Development Kit (CDK) to automate the deployment of an Amazon S3 bucket with specific configurations, including versioning and encryption."
mathjax = "tex-mml"
+++
# S3 Bucket with AWS

## Description

This project leverages the AWS Cloud Development Kit (CDK) to automate the deployment of an Amazon S3 bucket with specific configurations, including versioning and encryption. The setup is designed to be initiated within an AWS CodeCatalyst environment and utilizes AWS Cloud9 for code development. Further details are provided below.


## Prerequisites

Before you start, ensure you have the following prerequisites installed and configured:

* AWS CLI
* Node.js and npm
* AWS CDK Toolkit
* An AWS account and your credentials configured locally

## Setup Instructions
1. Begin by logging into `CodeCatalyst` and creating a new project. Choose the option to start from scratch and then proceed to establish a new environment within this project.
2. Open the newly created development environment through AWS Cloud 9.
3. Navigate to `AWS IAM` to set up a new user account. (If you have already have one, that's fine.)
4. For the new user, grant permissions by assigning the following policies: `IAMFullAccess`, `AmazonS3FullAccess`, `AmazonEC2ContainerRegistryFullAccess`, and `AWSLambda_FullAccess`.
5. Continue by adding inline policies for `CloudFormation` and `Systems Manager`.
6. Under Security Credentials, generate a new access key for the user.
7. Return to the Cloud 9 environment and enter the command `aws configure` in the terminal to set up your AWS credentials.
8. Create a new project directory by typing `mkdir YOUR_PROJECT_NAME`, then navigate into it with `cd` and initialize a new CDK project using the command `cdk init app --language=typescript`.
9. Use CodeWhisperer within the file **\lib\mini-proj-3-stack.ts**, prompting it with a comment to suggest code for an S3 bucket with versioning and encryption.
10. In the **\bin\mini-proj-3.ts** file, include a comment to prompt the addition of necessary variables to correctly create and deploy an S3 bucket.
11. Compile the TypeScript code to JavaScript with `npm run build`.
12. Generate the CloudFormation template by running `cdk synth`.
13. Deploy your stack to the default AWS account and region using `cdk deploy`.
14. Verify the deployment by checking for the new S3 bucket in the AWS management console.


## Main Project Structure
* `bin/`: Contains the entry point script for the CDK application.
* `lib/`: Includes the CDK stack definition that describes the AWS resources.


## Key Components
* **S3 Bucket**: An Amazon S3 bucket with versioning enabled and data encryption using AWS managed keys (KMS). Additional configurations include enforced SSL for data in transit and a policy for automatic bucket destruction upon stack deletion, suitable for testing purposes.

* **IAM Permissions**: The bucket is configured to grant read access to the root account, demonstrating how to apply permissions using AWS IAM.

## Cleanup
To avoid incurring future charges, remember to destroy the resources after you are done testing the application by running `cdk destroy`.

## Additional Information
For more details on AWS CDK, visit the [AWS CDK Developer Guide](https://docs.aws.amazon.com/cdk/v2/guide/home.html).

## S3 Bucket Screenshot
I added versioning by `enabling it via versioned: true` and added encryption by enabling it `encryption: cdk.aws_s3.BucketEncryption.S3_MANAGED`

![Function overview](https://gitlab.com/Johnnyee/idsweek3/-/wikis/uploads/e8faa9a42e850bf25ce39014ce6bef8d/1.png)
